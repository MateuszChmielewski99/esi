﻿using KNN.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace KNN.Extensions
{
    public static class ModelExtension
    {
        public static DiabetesObservation ToTestValue(this DiabetesObservation diabetesObservation) 
        {
            return new DiabetesObservation
            {
                Age = diabetesObservation.Age,
                BloodPreasure = diabetesObservation.BloodPreasure,
                History = diabetesObservation.History,
                Weight = diabetesObservation.Weight,
                Diagnose = null
            };
        }
    }
}
