﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace KNN.Alghoritm
{
    public class Knn
    {
        public void Classify<T>(T[] observations, T toClassify, Func<T, T, double> distanceCallculation, int k, string explainedVariableName)
        {
            var neighbours = observations
                .OrderBy(s => distanceCallculation.Invoke(s, toClassify))
                .Take(k);

            var votingResult = new Dictionary<double, int>();
            foreach (var n in neighbours)
            {
                var r = n.GetType().GetProperty(explainedVariableName).GetValue(n) as double?;
                if (votingResult.ContainsKey(r.Value))
                {
                    votingResult[r.Value]++;
                }
                else 
                {
                    votingResult.Add(r.Value, 1);
                }
            }

            var result = votingResult.Max(s => s.Value);
            double d = votingResult.FirstOrDefault(s => s.Value == result).Key;
            toClassify.GetType().GetProperty(explainedVariableName).SetValue(toClassify, d);
        }
    }
}
