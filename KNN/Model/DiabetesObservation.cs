﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KNN.Models
{
    public class DiabetesObservation
    {
        public double BloodPreasure { get; set; }
        public double Weight { get; set; }
        public double History { get; set; }
        public double Age { get; set; }
        public double? Diagnose { get; set; }

        public DiabetesObservation(double bloodPreasure, double weight, double history, double age, double? diagnose)
        {
            BloodPreasure = bloodPreasure;
            Weight = weight;
            History = history;
            Age = age;
            Diagnose = diagnose;
        }
        public DiabetesObservation()
        {
        }

        public override string ToString()
        {
            return $"Ciśnienie krwi {BloodPreasure}, waga {Weight}, historia {History}, Wiek {Age}, Diagnoza {Diagnose}";
        }
    }
}
