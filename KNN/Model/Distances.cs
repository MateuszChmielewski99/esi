﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace KNN.Models
{
    public class Distance
    {
        public static double Euclidean<T>(T a, T b) where T : new()
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            double result = 0;

            foreach (var property in properties) 
            {
                double? first = property.GetValue(a) is double ? property.GetValue(a) as double? : null;
                double? second = property.GetValue(b) is double ? property.GetValue(b) as double? : null;
               
                if (first == null && second == null)
                    throw new Exception("Comparing object with non-double values");

                if (!second.HasValue || !first.HasValue)
                    break;
                
                result += Math.Pow(first.Value - second.Value, 2);
            }
            return Math.Sqrt(result);
        }
    }
}
