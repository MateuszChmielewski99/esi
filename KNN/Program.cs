﻿using KNN.Alghoritm;
using KNN.Extensions;
using KNN.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KNN
{
    class Program
    {
        static void Main(string[] args)
        {
            var knn = new Knn();
            DiabetesObservation[] observations =
                {
                    new DiabetesObservation(2, 3, 1, 2, 1),
                    new DiabetesObservation(1,2,1,1,0),
                    new DiabetesObservation(3,3,2,2,1),
                    new DiabetesObservation(3,3,1,2,0),
                    new DiabetesObservation(2,3,1,1,1),
                    new DiabetesObservation(1,1,1,1,0),
                    new DiabetesObservation(1,1,2,1,0),
                    new DiabetesObservation(2,3,1,1,0),
                    new DiabetesObservation(3,3,2,2,1),
                    new DiabetesObservation(2,2,2,1,0),
                    new DiabetesObservation(2,3,1,2,1),
                    new DiabetesObservation(3,3,2,1,0),
                    new DiabetesObservation(3,2,1,2,1),
                    new DiabetesObservation(2,3,2,1,0),
                    new DiabetesObservation(3,3,2,2,1),
                    new DiabetesObservation(3,2,2,1,0)
                };


            var test = new DiabetesObservation[]
            {
                new DiabetesObservation(2,2,1,2,1),
                new DiabetesObservation(2,3,1,1,1),
                new DiabetesObservation(3,2,2,2,0),
                new DiabetesObservation(1,2,2,2,0),
            };

            double score = Test(observations, observations);
            Console.WriteLine(score);

            var toClassify = new DiabetesObservation(3, 3, 1, 2, null);

            knn.Classify<DiabetesObservation>(observations, toClassify, Distance.Euclidean, 5, "Diagnose");

            Console.WriteLine("Result: {0}", toClassify.Diagnose);
        }

        private static double Test(DiabetesObservation[] observations, DiabetesObservation[] test) 
        {
            double score = 0;
            var knn = new Knn();

            foreach (var ob in test)
            {
                var testVal = ob.ToTestValue();
                knn.Classify<DiabetesObservation>(observations, testVal, Distance.Euclidean, 5, "Diagnose");
                if (ob.Diagnose == testVal.Diagnose)
                {
                    score++;
                }
            }
            return score/test.Length;
        }
    }
}
